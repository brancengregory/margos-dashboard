setwd("~/Documents/margos/dashboard/")

library(tidyverse)
library(ggthemes)
library(readxl)
library(zoo)
library(fastDummies)
library(tidymodels)
library(modeltime)
library(timetk)
library(lubridate)
library(prophet)
library(pointblank)


theme_set(theme_bw())

set.seed(100)

##########Import and Cleaning###############

data <- read_excel("data/margos.xlsx", 
                   col_types = c("date", "text", "text", 
                                 "numeric", "numeric", "text", "numeric", 
                                 "numeric"))

# data <- read_csv("./dashboard/data/margos.csv", col_types = cols(DATE = col_date(format = "%m/%d/%y"), 
#                                                 PRICE = col_double(),
#                                                 PRODUCT = col_double()))

# data$DATE <- as.Date(data$DATE)
# data$NAME <- as.character(data$NAME)
# data$SERVICE <- as.factor(data$SERVICE)
# data$PRICE <- as.numeric(data$PRICE)
# data$PAYMENT <- as.numeric(data$PAYMENT)
# data$METHOD <- as.factor(data$METHOD)
# data$TIME <- as.numeric(data$TIME)
# data$PRODUCT <- as.numeric(data$PRODUCT)



summary(data)


####### Data Validation #############

pointblank::create_agent(
  tbl = data,
  tbl_name = "data"
) |>
  col_is_posix(vars(DATE)) |>
  col_vals_between(vars(TIME), 0, 8) |>
  col_vals_between(vars(PRICE), 0, 300) |>
  interrogate()


#######Infer Columns and Rearrange###########

data <- data %>%
  mutate(TIP = PAYMENT - PRICE) %>%
  select(DATE, NAME, SERVICE, TIME, PRICE, PAYMENT, TIP, METHOD) %>%
  filter(DATE >= ymd("2017-01-01") & DATE <= today()) |>
  mutate_if(is.numeric, ~replace_na(., 0)) |>
  drop_na()


#######Remove Outliers###############
d1 <- data |>
  summarise(across(where(is.numeric), ~quantile(., probs = c(0.25, 0.75), na.rm = T)))

d2 <- data |>
  summarise(across(where(is.numeric), function(x){
    10 * IQR(x, na.rm = T)
  }))

d_limits <- d1[2,] + d2

data <- data |>
  filter(TIME >= 0 & TIME <= d_limits$TIME,
         PRICE >= 0 & PRICE <= d_limits$PRICE,
         PAYMENT >= 0 & PAYMENT <= d_limits$PAYMENT,
         TIP >= 0 & TIP <= d_limits$TIP)
################Basic Plots###################

####Col Plot of Payment Method####
data %>%
  group_by(METHOD) %>%
  summarise(n = n()) %>%
  ggplot(aes(x = reorder(METHOD, n), y = n, fill = METHOD)) +
    geom_col() +
    ggtitle("# of Payments by Method") +
    ylab("# of Payments") +
    xlab("Payment Method") +
    coord_flip()

####Table of Frequent Fliers####
data %>%
  select(NAME) %>%
  group_by(NAME) %>%
  summarise(n = n()) %>%
  arrange(desc(n)) %>%
  head(n = 10) %>%
  ggplot(aes(x = reorder(NAME, n), y = n, fill = NAME)) +
    geom_col() +
    ggtitle("Frequent Fliers") +
    xlab("Client") +
    ylab("# of Appointments") +
    coord_flip()
    #theme(axis.text.x = element_text(angle = 45, hjust = 1))

####Table of Total Big Spenders####
data %>%
  select(NAME, PAYMENT) %>%
  group_by(NAME) %>%
  summarise(Total_Payments = sum(PAYMENT)) %>%
  arrange(desc(Total_Payments)) %>%
  head(n = 10) %>%
  ggplot(aes(x = reorder(NAME, Total_Payments), y = Total_Payments, fill = NAME)) +
    geom_col() +
    ggtitle("Total Big Spenders") +
    xlab("Client") +
    ylab("Total Spent ($)") +
    coord_flip()
    #theme(axis.text.x = element_text(angle = 45, hjust = 1))

####Table of Average Big Spenders####
data %>%
  select(NAME, PAYMENT) %>%
  group_by(NAME) %>%
  summarise(Weighted_Payments = sum(PAYMENT)/n()) %>%
  arrange(desc(Weighted_Payments)) %>%
  head(n = 10) %>%
  ggplot(aes(x = reorder(NAME, Weighted_Payments), y = Weighted_Payments, fill = NAME)) +
    geom_col() +
    ggtitle("Average Big Spenders") +
    xlab("Client") +
    ylab("Average Spent ($)") +
    coord_flip()
    #theme(axis.text.x = element_text(angle = 45, hjust = 1))

####Table of Top Tippers####
data %>%
  select(NAME, TIP) %>%
  group_by(NAME) %>%
  summarise(AVG_TIP = sum(TIP)/n()) %>%
  arrange(desc(AVG_TIP)) %>%
  head(n = 10) %>%
  ggplot(aes(x = reorder(NAME, AVG_TIP), y = AVG_TIP, fill = NAME)) +
    geom_col() +
    ggtitle("Top Tippers") +
    xlab("Client") +
    ylab("Average Tip ($)") +
    coord_flip()
    #theme(axis.text.x = element_text(angle = 45, hjust = 1))

####Scatter of Payments Across Dates####
data %>%
  ggplot(aes(x = DATE, y = PAYMENT, color = SERVICE)) +
    geom_point(aes(alpha = 0.2)) +
    ggtitle("Payments by Date") +
    xlab("Date") +
    ylab("Payment ($)")

####Daily Revenue Across Dates####
avg_daily_rev <- data %>%
  group_by(DATE) %>%
  summarise(DAILY_REVENUE = sum(PAYMENT)) %>%
  select(DAILY_REVENUE) %>%
  unlist() %>%
  na.omit() %>%
  mean
  
data %>%
  group_by(DATE) %>%
  summarise(DAILY_REVENUE = sum(PAYMENT)) %>%
  ggplot(aes(x = DATE, y = DAILY_REVENUE)) +
    geom_line() +
    geom_hline(yintercept = avg_daily_rev, col = "red", linetype = 2)

####Gauge of Gross Revenue YTD####
gross_rev_td <- data %>%
  filter(.$DATE > as.Date("2021-01-01")) %>%
  filter(.$DATE < as.Date("2022-01-01")) %>%
  select(PAYMENT) %>%
  sum()


####Revenue####

daily <- data %>%
  group_by(DATE, SERVICE) %>%
  summarise(HOURS = sum(TIME),
            REV = sum(PAYMENT),
            CHARGE = sum(PRICE)) %>%
  mutate(REVPERHOUR = REV/HOURS,
         CHARGEPERHOUR = CHARGE/HOURS,
         WEEK = week(DATE),
         MONTH = month(DATE),
         QTR = quarter(DATE),
         YEAR = year(DATE),
         WEEKDAY = wday(DATE),
         YEARDAY = yday(DATE))

daily %>%
  group_by(DATE) %>%
  ggplot(aes(x = DATE, y = REV)) +
    geom_col()

daily %>%
  group_by(YEARDAY, YEAR) %>%
  ggplot(aes(x = YEARDAY, y = REV)) +
    geom_col() +
    facet_grid(rows = vars(YEAR))

daily %>%
  group_by(WEEK, YEAR) %>%
  ggplot(aes(x = WEEK, y = REV)) +
    geom_col() +
    facet_grid(rows = vars(YEAR))

daily %>%
  group_by(MONTH, YEAR) %>%
  ggplot(aes(x = MONTH, y = REV)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(QTR, YEAR) %>%
  ggplot(aes(x = QTR, y = REV)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(WEEKDAY, YEAR) %>%
  ggplot(aes(x = WEEKDAY, y = REV)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(YEAR) %>%
  summarise(REV = sum(REV)) %>%
  ggplot(aes(x = YEAR, y = REV)) +
    geom_col()

daily %>%
  group_by(YEAR) %>%
  mutate(CUMSUM = cumsum(REV)) %>%
  ggplot(aes(x = YEARDAY, y = CUMSUM)) +
    geom_line() +
    facet_grid(rows = vars(YEAR))

daily %>%
  mutate(CUMSUM = cumsum(REV)) %>%
  ggplot(aes(x = DATE, y = CUMSUM)) +
  geom_line()


#######Hours Worked###########

daily %>%
  group_by(DATE) %>%
  ggplot(aes(x = DATE, y = HOURS)) +
  geom_col()

daily %>%
  group_by(YEARDAY, YEAR) %>%
  ggplot(aes(x = YEARDAY, y = HOURS)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(WEEK, YEAR) %>%
  ggplot(aes(x = WEEK, y = HOURS)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(MONTH, YEAR) %>%
  ggplot(aes(x = MONTH, y = HOURS)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(QTR, YEAR) %>%
  ggplot(aes(x = QTR, y = HOURS)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(WEEKDAY, YEAR) %>%
  ggplot(aes(x = WEEKDAY, y = HOURS)) +
  geom_col() +
  facet_grid(rows = vars(YEAR))

daily %>%
  group_by(YEAR) %>%
  summarise(HOURS = sum(HOURS)) %>%
  ggplot(aes(x = YEAR, y = HOURS)) +
  geom_col()


#########Pay Per Hour##############

daily %>%
  group_by(YEAR) %>%
  summarise(PAYPERHOUR = sum(REV)/sum(HOURS)) %>%
  ggplot(aes(x = YEAR, y = PAYPERHOUR)) +
    geom_col()

daily %>%
  group_by(MONTH) %>%
  summarise(PAYPERHOUR = sum(REV)/sum(HOURS)) %>%
  ggplot(aes(x = MONTH, y = PAYPERHOUR)) +
  geom_col()

daily %>%
  group_by(WEEK) %>%
  summarise(PAYPERHOUR = sum(REV)/sum(HOURS)) %>%
  ggplot(aes(x = WEEK, y = PAYPERHOUR)) +
  geom_col()

daily %>%
  group_by(WEEKDAY) %>%
  summarise(PAYPERHOUR = sum(REV)/sum(HOURS)) %>%
  ggplot(aes(x = WEEKDAY, y = PAYPERHOUR)) +
  geom_col()

daily %>%
  group_by(QTR) %>%
  summarise(PAYPERHOUR = sum(REV)/sum(HOURS)) %>%
  ggplot(aes(x = QTR, y = PAYPERHOUR)) +
  geom_col()

daily %>%
  group_by(YEARDAY) %>%
  summarise(PAYPERHOUR = sum(REV)/sum(HOURS)) %>%
  ggplot(aes(x = YEARDAY, y = PAYPERHOUR)) +
  geom_col()

daily %>%
  ggplot(aes(x = DATE, y = REVPERHOUR)) +
    geom_point(aes(color = SERVICE))

#########Pricing###########

data %>% 
  group_by(SERVICE) %>% 
  ggplot(aes(DATE, PRICE, color=SERVICE)) +
    geom_point(alpha = 0.8)

data %>% 
  group_by(SERVICE) %>% 
  ggplot(aes(DATE, PAYMENT, color=SERVICE)) +
    geom_point(alpha = 0.8)

data %>%
  group_by(SERVICE) %>%
  summarise(AVGPRICE = mean(PRICE)/n(),
            YEAR = year(DATE)) %>%
  ggplot(aes(reorder(SERVICE, AVGPRICE), AVGPRICE, fill=SERVICE)) +
    geom_col() +
    coord_flip() +
    facet_grid(rows = vars(YEAR))

#################Forecast ####################

model <- daily %>%
  summarise(REV = sum(REV)) %>%
  mutate(CUMSUM = cumsum(REV)) %>%
  select(DATE, CUMSUM) %>%
  rename(ds = DATE, y = CUMSUM) %>%
  prophet()

future <- make_future_dataframe(model, periods = 365)

forecast <- predict(model, future)

prophet::prophet_plot_components(model, forecast)

forecast %>%
  ggplot(aes(x = ds)) +
    geom_line(aes(y = yhat)) +
    geom_ribbon(aes(ymin = yhat_lower, ymax = yhat_upper, alpha=0.2))


samples <- predictive_samples(model, future)

#######Linear Model########
y <- data$PRICE
time <- data$TIME
type <- as.factor(data$SERVICE) %>%
  dummy_cols()

model <- lm(formula = y ~ time) %>%
  tidy()

model <- lm(formula = y ~ type$.data_Color)
summary(model)

model <- lm(formula = y ~ time + type$.data_Color + type$.data_Haircut)
summary(model)


#######Tidymodels/Modeltime#########

daily_revenue <- daily %>%
  summarise(REV = sum(REV)) %>%
  mutate(CUMREV = cumsum(REV))

splits <- initial_time_split(daily_revenue, prop = 0.75)

model_arima <- arima_reg() %>%
  set_engine(engine = "auto_arima") %>%
  fit(CUMREV ~ DATE, data = training(splits))

model_arima_boost <- arima_boost(
  min_n = 2,
  learn_rate = 0.015
) %>%
  set_engine(engine = "auto_arima_xgboost") %>%
  fit(CUMREV ~ DATE + as.numeric(DATE) + factor(month(DATE, label = T), ordered = F),
      data = training(splits))

model_prophet <- prophet_reg() %>%
  set_engine(engine = "prophet") %>%
  fit(CUMREV ~ DATE, data = training(splits))

model_prophet_boost <- prophet_boost() %>%
  set_engine(engine = "prophet_xgboost") %>%
  fit(CUMREV ~ DATE, data = training(splits))

model_lm <- linear_reg() %>%
  set_engine("lm") %>%
  fit(CUMREV ~ as.numeric(DATE) + factor(month(DATE, label = T), ordered = F),
      data = training(splits))

models <- modeltime_table(
  model_arima,
  model_arima_boost,
  model_prophet,
  model_prophet_boost,
  model_lm
)

calibration <- models %>%
  modeltime_calibrate(new_data = testing(splits))

calibration %>%
  modeltime_forecast(
    new_data = testing(splits),
    actual_data = daily_revenue
  ) %>%
  plot_modeltime_forecast(
    .legend_max_width = 25,
    .interactive = T
  )

calibration %>%
  modeltime_accuracy() %>%
  table_modeltime_accuracy(
    .interactive = T
  )

refit <- calibration %>%
  modeltime_refit(data = daily_revenue)

refit %>%
  modeltime_forecast(h = "1 year", actual_data = daily_revenue) %>%
  plot_modeltime_forecast(
    .legend_max_width = 25, # For mobile screens
    .interactive      = T
  )

##########Consumer Price Index#############

# cpi <- read_csv("data/cpi.csv", na = "N/A",
#                 col_types = cols(`Series ID` = col_character(),
#                                   Year = col_character(),
#                                   Period = col_character(),
#                                   Label = col_character(),
#                                   Value = col_double(),
#                                   `1-Month % Change` = col_double(),
#                                   `3-Month % Change` = col_double(),
#                                   `6-Month % Change` = col_double(),
#                                   `12-Month % Change` = col_double()
#                                 )) |>
#   filter(!str_detect(Label, "Half")) |>
#   select(
#     -`Series ID`,
#     -ends_with("Change")
#   ) |>
#   clean_names() |>
#   mutate(period = str_sub(period, start = -2),
#          date = str_c(year, period, "01", sep = "-") |> ymd())

#### Fred Series CUUR0000SEGC

cpi <- read_csv("data/cpi.csv", na = "N/A")

base_date <- ymd("2017-01-01")
base_value <- cpi |>
  filter(date == base_date)
index_transform_factor <- 100/cpi[930,]$Value

###Change base to Jan 2017###
cpi <- cpi %>%
  mutate(transformed_val = Value * index_transform_factor)

cpi$Label %>%
  as.Date(format = "%Y")

cpi %>%
  filter(Year > as.Date("2017-01-01")) %>%
  ggplot(aes(x = Year, y = Value)) +
    geom_point()


###############
#####Look at what we would have made in April using forecast
#####Look at Rolling graph of clientele, additions and falloff





